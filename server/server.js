net = require('net');
 
var clients = [];
var nick = {}; //array responsável pelo armazenamento dos nicknames
console.log("Rodando na porta 5000\n");
net.createServer(function (socket) {
  socket.name = socket.remoteAddress + ":" + socket.remotePort;
  clients.push(socket);
  socket.on('data', function (data) {
    var command = data.toString("utf-8").trim().split(' ');
    switch(command[0].toUpperCase()){ //switch principal. Verifica os comandos enviados
      case "NICK": //comando para mudar o nick, use NICK <nickname>
         if (typeof(nick[command[1]]) != "undefined"){ //passa o <nickname> como chave da array e verifica se o valor foi definido.
            socket.write("Este usuário já existe.\n");
          }
        else{
          try{
            delete nick[socket.name.split("|")[1]];
          }
          finally{
            nick[command[1]] = socket.remoteAddress; //associa o nick ao endereço ip na array
            socket.name = socket.remoteAddress + ":" + socket.remotePort;
            socket.name += "|" + command[1];
            broadcast(command[1] + " se juntou ao chat.\n");
           }
        }
        break;
      case "USERIP": //comando para saber o ip de um usuário. Use: USERIP <nickname>
         if (typeof(nick[command[1]]) == "undefined"){
            socket.write("Este usuário não existe.\n");
         }
         else{
           socket.write(nick[command[1]] + "\n");
         }
         break;
      case "QUIT": //comando para se desconectar. Deleta o usuário da array associativa.
        var nickname = socket.name.split("|")[1];
        delete nick[socket.name.split("|")[1]];
        clients.splice(clients.indexOf(socket), 1);
        socket.destroy();
        broadcast(nickname + " saiu do chat.\n");
        break;
      case "USERS": //comando para receber nicknames dos usuários conectados. 
        var users = "";
        for(var i in nick){
          users += i + "\n";
        }
        socket.write(users);
        break;
      case "PRIVMSG": //comando para mandar msg privada.Não implementado
        break;
      default: //senão for um comando simplismente envia para todos os usuários a mensagem enviada.
        broadcast(socket.name.split("|")[1] + "->" + data.toString("utf-8"));
        break;
    }
  });
 
  socket.on('end', function () {
        clients.splice(clients.indexOf(socket), 1);
  });
 
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      if (client === sender) return;
      client.write(message);
    });
    process.stdout.write(message)
  }
}
).listen(5000);
